import pymysql.cursors
from auth import Auth
from trip import *
from google_maps import GeoTools
import tkinter
from tkinter import messagebox

'''
    Class to access sql environment. In this case, it's a mysql database.
    ** Do not create an instance. Use Repository.data to access this class.
'''
class DBAccess:
    def __init__(self, host, port, user, password, db):
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.db = db
    
    '''
        Returns an active connection to the defined MySql environment defined in constructor.
        **Do not access this function from outside. For internal use only.
    '''
    def __open(self):
        return pymysql.connect(host=self.host,
                               port=self.port,
                               user=self.user,
                               password=self.password,
                               db=self.db,
                               cursorclass=pymysql.cursors.DictCursor)
    '''
        Function to isolate execute requirement. 
        At this moment, it does not do much since both requirement (update and query) are the same
        **Do not access this function from outside. For internal use only.
    '''
    def __run(self, cursor, sql, params):
        cursor.execute(sql, params)
        print(cursor._executed)
    
    '''
        Returns a resultset containing all rows meeting the param sql requirement.
        **Do not access this function from outside. For internal use only.
    '''
    def __runQuery(self, sql, params):
        try:
            connection = self.__open()
            with connection.cursor() as cursor:
                self.__run(cursor, sql, params)
                result = cursor.fetchall()
                return result
        except ValueError:
            print(args)
        finally:
            connection.close()

    '''
        Returns the inserted id from the insert operation. Won't return a valid key if it is an Update. In case of failure, will be -1.
        **Do not access this function from outside. For internal use only.
    '''
    def __runUpdate(self, sql, params):
        try:
            connection = self.__open()
            with connection.cursor() as cursor:
                self.__run(cursor, sql, params)
                r = cursor.lastrowid
            connection.commit()
            return r
    
        except ValueError:
            print(args)
            connection.rollback()
        finally:
            connection.close()

    '''
        Check if an user is defined in the database for a givien username and password
        Return an auth object, which mimics the User table structure, but has a wider functionality in the application
        Return None if the row doesn't exist in db
    '''
    def authUser(self, params):

        sql = 'SELECT * FROM `User` WHERE userName=%s AND password=%s'
        res = self.__runQuery(sql, params)
        ath = Auth('','')
        if(len(res) == 1):
            ath.id = res[0]['id']
            ath.log = res[0]['userName']
            ath.pwd = res[0]['password']
            ath.name = res[0]['lastName']
            ath.email = res[0]['email']
        return ath

    '''
        Adds a User to the table if it does not exists already.
        returns the resulting id. if > 0, the insert was a success
    '''
    def regUser(self, auth):

        sql = 'INSERT INTO User(userName, password, lastName, email, birthDate) VALUES (%s, %s, %s, %s, %s)'
        params = (auth.log, auth.pwd, auth.name, auth.email, auth.birthDay)
        resId = self.__runUpdate(sql, params)
        if(resId > 0):
            auth.id = resId
        return resId
    
    '''
        Adds row in Driver for an already existing user (the userAuth object in parameter)
        returns the id of the user, if id < 0 the insert failed
    '''
    def regDriver(self, userAuth, licenceNo, experienceYears):

        sql = "INSERT INTO Driver (id, licenceNo, experienceYears) VALUES (%s, %s, %s);"
        params = [userAuth.id, licenceNo, experienceYears]
        resId = self.__runUpdate(sql, params)
        if resId > 0:
            msg = "Success"
        else:
            msg = "Insertion error"
        tkinter.messagebox.showinfo(msg)
        return resId

    '''
        Adds a row in Model
        return the id of the model, if id < 0 the insert failed
    '''
    def addModel(self, modelName, modelYear, kmPrice):
        sql = "INSERT INTO Model (modelName, modelName, kmPrice) VALUES (%s, %s, %s);"
        params = [modelName, modelYear,kmPrice]
        resId = self.__runUpdate(sql, params)
        if resId > 0:
            msg = "Success"
        else:
            msg = "Insertion error"
        tkinter.messagebox.showinfo(msg)
        return resId
        
    '''
        Adds a row in Car
        return the id of the car, if id < 0 the insert failed
    '''
    def addCar(self, immat, driverId, seatsNb, storagePlace, modelId, initialAddress):
        sql = """INSERT INTO Car (immat, driverId, seatsNb, storagePlace, modelId, lat, lng) 
                    VALUES (%s, %s, %s, %s, %s, %s, %s);"""
        geo = GeoTools();
        (lat, lng) = geo.getLatLng(initialAddress)
        params = [immat, driverId, seatsNb, storagePlace, modelId, lat, lng]
        resId = self.__runUpdate(sql, params)
        if resId > 0:
            msg = "Success"
        else:
            msg = "Insertion error"
        tkinter.messagebox.showinfo(msg)
        return resId
    '''
        Insert a row in Trip
        This is only a driver action
    '''
    def addTrip(self, carId, date):
        sql = "INSERT INTO Trip (carId, date) VALUES (%s, %s);"
        params = [carId, date]
        resId = self.__runUpdate(sql, params)
        if resId > 0:
            msg = "Success"
        else:
            msg = "Insertion error"
        tkinter.messagebox.showinfo(msg)
        return resId

    def addStop(self, tripId, seqOrder, address):
        sql = """INSERT INTO Stop (tripId, seqOrder, address, lat, lng)
                    VALUES (%s, %s, %s, %s, %s);"""
        geo = GeoTools();
        (lat, lng) = geo.getLatLng(initialAddress)
        params = [tripId, seqOrder, address, lat, lng]
        resTripId, = self.__runUpdate(sql, params)
        if resId > 0:
            msg = "Success"
        else:
            msg = "Insertion error"
        tkinter.messagebox.showinfo(msg)
        return resId

    '''
        Accept a stop in a trip
        This is only a driver action
    '''
    def acceptStop(tripId, seqOrder):
        sql = "UPDATE Stop SET state = %s WHERE tripId = %s AND seqOrder = %s"
        #if (passengerAccepted):
        #   state = "accepted"
        #else:
        #    state = "refused"
        #params = [state, tripId, seqOrder]
        #resUserId, = self.__runUpdate(sql, params)

    '''
        Check if a stop has been accepted
        Return False if the stop has been refused or still waiting"
    '''

    def checkIfAccepted(tripId, seqOrder):
        sql = "SELECT state FROM Stop WHERE tripId = %s AND seqOrder = %s"
        params = [tripId, seqOrder]
        res = self.__runQuery(sql, params)
        if len(res) == 1:
            return res[0]['state'] == 'accepted'

    '''
        Adds a row to Travel:
        Only if the two stops submitted are already accepted
    '''
    def addTravel(self, userId, tripId, departureOrder, arrivalOrder, reservedSeatsNb):
        if not checkIfAccepted(tripId, departureOrder) or not checkIfAccepted(tripId, arrivalOrder):
            tkinter.messagebox.showinfo("Error: at least one of the stops hasn't been accepted")
            return;

        sql = """INSERT INTO Travels (userId, tripId, departureOrder, arrivalOrder, reservedSeatsNb)
                    VALUES (%s, %s, %s, %s, %s);"""
        params = [userId, tripId, departureOrder, arrivalOrder, reservedSeatsNb]
        resUserId, = self.__runUpdate(sql, params)
        if resUserId > 0:
            msg = "Success"
        else:
            msg = "Insertion error"
        tkinter.messagebox.showinfo(msg)
        return resId

    '''
        Check if the database already contains a user for the given username and / or email
    '''
    def checkIfUserExists(self, userName, email):
        sql = 'SELECT * FROM `User` WHERE userName=%s OR email = %s'
        res = self.__runQuery(sql, (userName, email))
        return len(res) > 0

    '''
        internal fetchTrips.
        **Do not access this function from outside. For internal use only.
        
    '''
    def __fetchTripsForUser(self, sql, params):
        res = self.__runQuery(sql, params)
        trips = []
        for entry in res:
            trip = Trip(entry['carId'], entry['date'], entry['totalDistance'])
            trip.id = entry['id']
            trip.stops = self.fetchStops(trip)
            if 'isPaid' in entry:
                trip.isPaidForUser = entry['isPaid']
            else:
                trip.isPaidForUser = False
            trips.append(trip)
        return trips

    '''
        Fetch all trips available for the current active user. If the current user is a driver, we will remove all of the trips he owns. We also check if there are any available seats remaining
        Returns a list of trips with its stops
    '''
    def fetchTripsForUser(self, auth):
        sql = 'SELECT * FROM Trip AS t WHERE (SELECT IFNULL(SUM(reservedSeatsNb),0) FROM Travels as ut WHERE ut.tripId = t.id) < (SELECT seatsNb FROM Car WHERE id = t.carId) AND (SELECT driverId FROM Car AS v WHERE v.id = t.carId) != %s AND CURDATE() <= t.date'
        params = (auth.id)
        return self.__fetchTripsForUser(sql, params)
    
    '''
        Fetch filtered trips available for the current active user. If the current user is a driver, we will remove all of the trips he owns. We also check if there are any available seats remaining
        Returns a list of trips with its stops
    '''
    def fetchFilteredTripsForUser(self, auth, filters):
        baseSql = """ SELECT * FROM Trip AS t
                  JOIN Car AS c ON t.carId = c.id
                  WHERE (SELECT COUNT(*) FROM Travels as ut WHERE ut.tripId = t.id) < (SELECT seatsNb FROM Car WHERE id = t.carId) AND (SELECT driverId FROM Car AS v WHERE v.id = t.carId) != %s AND CURDATE() <= t.date """
        params = []
        params.append(auth.id)
        if(len(filters) > 0):
            filterSql = ''
            for filter in filters:
                filterSql += filter.filterFormat.format(filter.association, filter.key, filter.compareSymbol)
                params.append(filter.value)
            baseSql += filterSql
        return self.__fetchTripsForUser(baseSql, params)
    
    '''
        Fetch all trips currently active. A trip is active if its 
    '''
    def fetchActiveTrips(self):
        sql = 'SELECT * FROM Trip AS t WHERE CURRENT_TIMESTAMP >= t.date'
        res = self.__runQuery(sql, ())
        trips = []
        for entry in res:
            trip = Trip(entry['carId'], entry['date'], entry['totalDistance'])
            trip.id = entry['id']
            trip.stops = self.fetchStops(trip)
            trips.append(trip)
        return trips

    def fetchTripAndPaidStatus(self, auth):
        sql = 'SELECT * FROM Trip AS t JOIN Travels AS tr ON t.id = tr.tripId WHERE tr.userId = %s AND tr.paymentStatus != 1'
        params = (auth.id)
        return self.__fetchTripsForUser(sql, params)

    '''
        Fetch all stops for a given trip
        Returns a list of stops
    '''
    def fetchStops(self, trip):
        sql = 'SELECT * FROM Stop as a WHERE a.tripId = %s'
        res = self.__runQuery(sql, (trip.id))
        stops = []
        for entry in res:
            
            stop = Stop(trip, entry['seqOrder'], entry['lat'], entry['lng'], entry['address'], False if entry['checked'] == 0 else True)
            stops.append(stop)
        return stops
		
    def getUserTripPrice(self, userId, tripId):
        #sql = 'SELECT lat, lng FROM Stop WHERE tripId = %s UNION SELECT reservedSeatsNb FROM Travels WHERE userId = %s AND tripId = %s UNION SELECT kmPrice FROM Model WHERE id = (SELECT modelId FROM Car WHERE id = %s)'
        sql = """SELECT * FROM Travels AS t
                    INNER JOIN (Stop AS s, Model as m) ON
                        t.tripId = %s AND t.userId = %s
                        AND (t.departureOrder = s.seqOrder OR t.arrivalOrder = s.seqOrder)
                        AND s.tripId = t.tripId
                        AND m.id =
                            (SELECT modelId FROM Car WHERE id =
                                (SELECT carId from Trip WHERE id = t.tripId));"""
        params = [userId, tripId]
        coord = []
        res = self.__runQuery(sql, params)
        if(len(res) != 2):
            print("Error, did not get 2 stops for user trip!")
            return -1
        for entry in res:
            coord.append((entry['lat'], entry['lng']))
            #print("lat={} lng={}".format(entry['lat'], entry['lng']))
        kmPrice = float(res[0]['kmPrice'])
        geo = GeoTools()
        price = geo.getDistance(coord[0], coord[1])/1000 * kmPrice
        #print("Prix: {}".format(price))
        tkinter.messagebox.showinfo("Prix du trajet", "Le coût de votre trajet est de {0:.2f}$".format(price))
        return price

    def __makePaymentPaymentTable(self, userId, tripId):
        sql = 'INSERT INTO Payment (userId, tripId, `date`) VALUES(%s, %s, now())'
        print("id={} trip={}\n".format(userId, tripId))
        params = [userId, tripId]
        return self.__runUpdate(sql, params)

    def __makePaymentTravelsTable(self, userId, tripId):
        sql = 'UPDATE Travels SET paymentStatus = 1 WHERE userId = %s AND tripId = %s'
        params = [userId, tripId]
        self.__runUpdate(sql, params)

    def makePaymentCredit(self, userId, tripId, cardNo, expirationDate, name, address):
        id = self.__makePaymentPaymentTable(userId, tripId)
        sql = 'INSERT INTO Credit (id, cardNo, expirationDate, `name`, address) VALUES(%s, %s, %s, %s, %s)'
        params = [id, cardNo, expirationDate, name, address]
        self.__runUpdate(sql, params)
        self.__makePaymentTravelsTable(userId, tripId)

    def makePaymentPaypal(self, userId, tripId, accountName):
        id = self.__makePaymentPaymentTable(userId, tripId)
        sql = 'INSERT INTO Paypal (id, accountName) VALUES(%s, %s)'
        params = [id, accountName]
        self.__runUpdate(sql, params)
        self.__makePaymentTravelsTable(userId, tripId)

    def checkPaymentMade(self, userId, tripId):
        sql = 'SELECT paymentStatus FROM Travels WHERE paymentStatus = 1 AND userId = %s AND tripId = %s'
        params = [userId, tripId]
        res = self.__runQuery(sql, params)
        if(len(res) == 0):
            return False
        return True
'''
    Class with one static var, "data", to expose the db access implementation.
'''
class Repository:
    data = DBAccess('localhost', 8889, 'root', 'root', 'SPT2')
    #data = DBAccess('localhost', 3306, 'root', 'admin', 'SPT')

class SqlFilter:
    def __init__(self, association, value, key, compareSymbol, filterFormat):
        self.association = association
        self.value = value
        self.key = key
        self.compareSymbol = compareSymbol
        self.filterFormat = filterFormat

    def __str__(self):
        return 'association:{}, value:{}, key:{}, compareSymbol:{}, filterFormat[ {} ]'.format(self.association, self.value, self.key, self.compareSymbol, self.filterFormat)
