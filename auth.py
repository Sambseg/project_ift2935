'''
    The Auth class mimics the User table structure, but has a wider functionality in the application
'''
class Auth:
    def __init__(self, log, pwd):
        self.id = -1
        self.log = log
        self.pwd = pwd
        self.authState = False
        self.email = ''
        self.name = ''
        self.birthDay = ''

    def __str__(self):
        return 'Id:{}, Log:{}, Nom:{}, Courriel:{}'.format(self.id, self.log, self.name, self.email)
