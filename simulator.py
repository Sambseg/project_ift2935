import threading
import time
from trip import Trip
from conn import Repository

class SimulatorThread (threading.Thread):
    def __init__(self, threadID, name, sleepTime):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.sleepTime = sleepTime
        self.runState = True

    def run(self):
        print ("Starting " + self.name)
        simulate(self)
        print ("Exiting " + self.name)


def simulate(self):
    while self.runState:
        if(self.runState):
            print ("%s: %s" % (self.name, time.ctime(time.time())))
            t = Trip(0, 0, 0)
            t.id = 7
            trips = Repository.data.fetchActiveTrips()
            for trip in trips:
                print(trip)
        time.sleep(self.sleepTime)
