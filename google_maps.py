import googlemaps
import urllib.request
from datetime import datetime

class GeoTools:
	def __init__(self):
		self.geoclient = googlemaps.Client(key="AIzaSyB8m2qlE7I3wuizQwax6VMTq96FE5CFhhE")
		self.distclient = googlemaps.Client(key="AIzaSyDBRbqhaSYOvE7d4KnN-tY5oLWSAy_XwYw")
	
	#exemple: getLatLng("Place Ville-Marie, Montreal, QC")
	#retourne un tuple (lat, lng)
	def getLatLng(self, address):
		data = self.geoclient.geocode(address)
		if not data:
			print("Google Maps Geocode API got 0 results!")
			return (-1, -1)
		latlng = data[0]["geometry"]["location"]
		return (latlng["lat"], latlng["lng"])
	
	#exemple: getDuration((12, 13), (14, 15))
	#prend en parametre 2 tuples de forme (lat, lng)
	#retourne la duree du trajet sen minutes
	def getDuration(self, latLng1, latLng2):
		data = self.distclient.distance_matrix(latLng1, latLng2)
		elements = data["rows"][0]["elements"][0]
		if elements["status"] != "OK":
			print("Google Maps Distance Matrix API got 0 results!")
			return -1
		return elements["duration"]["value"]/60
	
	#exemple: getDistance((12, 13), (14, 15))
	#prend en parametre 2 tuples de forme (lat, lng)
	#retourne la distance du trajet en metres
	def getDistance(self, latLng1, latLng2):
		data = self.distclient.distance_matrix(latLng1, latLng2)
		elements = data["rows"][0]["elements"][0]
		if elements["status"] != "OK":
			print("Google Maps Distance Matrix API got 0 results!")
			return -1
		return elements["distance"]["value"]
