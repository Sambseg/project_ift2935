# ------------------------------------------------------------
# spt_script.sql
# Script de définition des données de SPT
# 
#
# Script généré avec la configuration suivante :
#
# Hôte: localhost (MySQL 5.7.18)
# Base de données: SPT
# 
# ------------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT= @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS= @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION= @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS= @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS= 0 */;
/*!40101 SET @OLD_SQL_MODE= @@SQL_MODE, SQL_MODE= 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES= @@SQL_NOTES, SQL_NOTES=0 */;


# Définition de la table User (Usager)
# ------------------------------------------------------------

DROP TABLE IF EXISTS `User`;
CREATE TABLE `User` (

  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lastName` varchar(30) NOT NULL,
  `userName` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(100) DEFAULT NULL, # adresse facultative
  `signUpDate` datetime NOT NULL DEFAULT NOW(),
  `birthDate` date NOT NULL,
  
  CHECK (`birthDate` < NOW()),
  CHECK (DATEDIFF(NOW(), `birthDate`) / 365.25 >= 16), # usager a plus de 16 ans

  PRIMARY KEY (`id`),
  UNIQUE KEY `userName` (`userName`),
  UNIQUE KEY `email` (`email`)

) ENGINE= InnoDB DEFAULT CHARSET= utf8;


# Définition de la table Driver (Conducteur)
# ------------------------------------------------------------

# methode pour valider l'age du conducteur

DROP PROCEDURE IF EXISTS `get_driver_bday`;
 CREATE PROCEDURE `get_driver_bday`(id_driver int)
  SELECT `birthDate` FROM `User` WHERE `id` = id_driver;
    
DROP TABLE IF EXISTS `Driver`;
CREATE TABLE `Driver` (

  `id` int(11) NOT NULL AUTO_INCREMENT,
  `licenceNo` int(11) NOT NULL,
  `experienceYears` int(11) NOT NULL,

  CHECK (DATEDIFF(NOW(), `get_driver_bday`()) / 365.25 >= 18), # conducteur a plus de 18 ans
  CHECK (`experienceYears` >= 3),

  PRIMARY KEY (`id`),
  UNIQUE KEY `licenceNo` (`licenceNo`),
  CONSTRAINT `driver_ibfk_1` FOREIGN KEY (`id`) REFERENCES `User` (`id`) ON DELETE CASCADE

) ENGINE= InnoDB DEFAULT CHARSET= utf8;

# Définition de la table Model (Modèle)
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Model`;
CREATE TABLE `Model` (

  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modelName` varchar(20) NOT NULL,
  `modelYear` year(4) NOT NULL,
  `kmPrice` decimal(4,3) NOT NULL, # prix/km en dollars, exemple 0.098 $/km

  CHECK (YEAR(NOW()) - `modelYear` < 25),

  PRIMARY KEY (`id`)

) ENGINE= InnoDB DEFAULT CHARSET= utf8;

# Définition de la table Car (Voiture)
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Car`;
CREATE TABLE `Car` (

  `id` int(11) NOT NULL AUTO_INCREMENT,
  `immat` char(6) NOT NULL,
  `driverId` int(11) NOT NULL,
  `seatsNb` int(11) NOT NULL,
  `storageSpace` enum('small','medium','large') NOT NULL,
  `modelId` int(11) NOT NULL,
  `lat` float(12,9) NOT NULL, # latitude et longitude en degrés décimaux
  `lng` float(12,9) NOT NULL,

  PRIMARY KEY (`id`),
  UNIQUE KEY `immat` (`immat`),
  KEY `driverId` (`driverId`),
  KEY `modelId` (`modelId`),
  CONSTRAINT `car_ibfk_1` FOREIGN KEY (`driverId`) REFERENCES `Driver` (`id`) ON DELETE CASCADE,
  CONSTRAINT `car_ibfk_2` FOREIGN KEY (`modelId`) REFERENCES `Model` (`id`)

) ENGINE= InnoDB DEFAULT CHARSET= utf8;

# Définition de la table Trip (Trajet)
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Trip`;
CREATE TABLE `Trip` (

  `id` int(11) NOT NULL AUTO_INCREMENT,
  `carId` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `totalDistance` float NOT NULL DEFAULT 0,
  `totalTime` time NOT NULL DEFAULT '00:00:00', # attributs sont calculés grace à l'API Google Maps

  CHECK (`date` >= NOW()),

  PRIMARY KEY (`id`),
  KEY `carId` (`carId`),
  CONSTRAINT `trip_ibfk_1` FOREIGN KEY (`carId`) REFERENCES `Car` (`id`) ON DELETE CASCADE

) ENGINE= InnoDB DEFAULT CHARSET= utf8;

# Définition de la table Stop (Arret)
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Stop`;
CREATE TABLE `Stop` (

  `tripId` int(11) NOT NULL, # id du trajet auquel il est rattaché
  `seqOrder` int(11) NOT NULL, # ordre séquentiel dans le trajet: 1, 2 etc.
  `address` text NOT NULL,
  `lat` float(12,9) NOT NULL, # latitude et longitude de l'adresse en degrés décimaux
  `lng` float(12,9) NOT NULL,
  `state` enum('accepted','refused','waiting') NOT NULL DEFAULT 'waiting',
  # demande d'acceptation de l'arrêt dans le trajet : le conducteur décide
  `checked` boolean NOT NULL DEFAULT 0,
  # indique si la voiture a traversé cette arrêt une fois le trajet démarré
  PRIMARY KEY (`tripId`,`seqOrder`),
  CONSTRAINT `stop_ibfk_1` FOREIGN KEY (`tripId`) REFERENCES `Trip` (`id`) ON DELETE CASCADE

) ENGINE= InnoDB DEFAULT CHARSET= utf8;

# Définition de la table Travels (Voyage)
# ------------------------------------------------------------

# methode pour avoir le nombre sièges réservés par les passagers d'un trajet

DROP PROCEDURE IF EXISTS `get_reserved_places`;
CREATE PROCEDURE `get_reserved_places`(id_trip int)
	SELECT SUM(reservedSeatsNb) FROM Travels WHERE tripId = id_trip GROUP BY tripId;

# methode pour avoir le nombre de sièges de la voiture d'un trajet

DROP PROCEDURE IF EXISTS `get_available_places`;
CREATE PROCEDURE `get_available_places`(id_trip int)
 	SELECT `seatsNb` FROM `Car` WHERE `id` =
   (SELECT `carId` FROM `Trip` WHERE `id` = id_trip);

DROP TABLE IF EXISTS `Travels`;
CREATE TABLE `Travels` (

  `userId` int(11) NOT NULL, 
  `tripId` int(11) NOT NULL, 
  `departureOrder` int(11) DEFAULT NULL, # ordre séquentiel de l'arrêt de départ et d'arrivée
  `arrivalOrder` int(11) DEFAULT NULL,
  `reservedSeatsNb` int(10) NOT NULL,
  `paymentStatus` boolean NOT NULL DEFAULT 0, # vaut true ssi le paiement a été effectué par l'usager

  CHECK (`departureOrder` < `arrivalOrder`),
  CHECK (`get_available_places`(`tripId`) - `get_reserved_places`(`tripId`) >= `reservedSeatsNb`), # ne peut réserver si pas assez de place

  PRIMARY KEY (`userId`,`tripId`),
  KEY `tripId` (`tripId`,`departureOrder`),
  KEY `tripId_2` (`tripId`,`arrivalOrder`),
  CONSTRAINT `travels_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `User` (`id`) ON DELETE CASCADE,
  CONSTRAINT `travels_ibfk_2` FOREIGN KEY (`tripId`) REFERENCES `Trip` (`id`) ON DELETE CASCADE,
  CONSTRAINT `travels_ibfk_3` FOREIGN KEY (`tripId`, `departureOrder`) REFERENCES `Stop` (`tripId`, `seqOrder`) ON DELETE CASCADE,
  CONSTRAINT `travels_ibfk_4` FOREIGN KEY (`tripId`, `arrivalOrder`) REFERENCES `Stop` (`tripId`, `seqOrder`) ON DELETE CASCADE
  
) ENGINE= InnoDB DEFAULT CHARSET= utf8;

# Définition de la table Payment (Paiement)
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Payment`;
CREATE TABLE `Payment` (

  `id` int(11) NOT NULL AUTO_INCREMENT, # id de l'usager et id du trajet emprunté (voyage)
  `userId` int(11) DEFAULT NULL,
  `tripId` int(11) DEFAULT NULL,
  `date` datetime NOT NULL DEFAULT NOW(),

  PRIMARY KEY (`id`),
  KEY `userId` (`userId`,`tripId`),
  CONSTRAINT `payment_ibfk_1` FOREIGN KEY (`userId`, `tripId`) REFERENCES `Travels` (`userId`, `tripId`) ON DELETE SET NULL

) ENGINE= InnoDB DEFAULT CHARSET= utf8;

# Définition de la table Credit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Credit`;
CREATE TABLE `Credit` (

  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cardNo` varchar(12) NOT NULL,
  `expirationDate` date NOT NULL,
  `name` varchar(30) NOT NULL,
  `address` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `credit_ibfk_1` FOREIGN KEY (`id`) REFERENCES `Payment` (`id`) ON DELETE CASCADE

) ENGINE= InnoDB DEFAULT CHARSET= utf8;

# Définition de la table Paypal
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Paypal`;
CREATE TABLE `Paypal` (

  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accountName` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `paypal_ibfk_1` FOREIGN KEY (`id`) REFERENCES `Payment` (`id`) ON DELETE CASCADE

) ENGINE= InnoDB DEFAULT CHARSET= utf8;

/*!40111 SET SQL_NOTES= @OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE= @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS= @OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT= @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS= @OLD_CHARACTER_SET_RESULTS */;
