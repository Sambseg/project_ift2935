class Trip:
    def __init__(self, carId, date, totalDistance):
        self.id = -1
        self.carId = carId
        self.date = date
        self.stops = []
        self.totalDistance = totalDistance
        self.isPaidForUser = False
    
    def __str__(self):
        return 'id:{}, carId:{}, totalDistance:{}, date:{}'.format(self.id, self.carId, self.totalDistance, self.date)

class Stop:
    def __init__(self, trip, seqOrder, lat, lng, address, checked):
        self.trip = trip
        self.seqOrder = seqOrder
        self.lat = lat
        self.lng = lng
        self.address = address
        self.checked = checked
    
    def __str__(self):
        return 'associatedTripInf:[id:{}, carId:{}], seqOrder:{}, lat:{}, long:{}, adress:{}, checked:{}'.format(self.trip.id, self.trip.carId, self.seqOrder, self.lat, self.lng, self.address, self.checked)



