#!/usr/bin/env python3

from tkinter import *
from tkinter import ttk
from tkinter import messagebox
from auth import Auth
from conn import Repository
from conn import SqlFilter
from helper import showMessage
from helper import validateDate
from trip import *
from simulator import SimulatorThread

'''
    App main state. Contains auth status and events
'''
class AppCore:
    def __init__(self):
        self.auth = Auth("","");
        self.actIdx = 0
        self.root = Tk()
        self.root.title("Application SPT - Gestion de transport")
        self.views = []
        self.__initCycle()
        self.simulator = SimulatorThread(1, "SimulatorT", 5)
        # Start simulator
        self.simulator.start()
        self.root.mainloop()
        # Kill simulator when window is closed
        self.simulator.runState = False
   
    def __initCycle(self):
        self.makeConnectView()

    '''
        Display f in the app window
    '''
    def __raiseView(self, f):
        f.frame.mainframe.tkraise()

    '''
        Add a view to stackView and display it in the current app window
    '''
    def stackView(self, f):
        self.views.append(f)
        self.__raiseView(f)
    '''
        Remove the top view from current stack (displayed view) and unbind all of its event.
        Display the next view instead
    '''
    def unstackView(self):
        lView = self.views.pop()
        for actString in lView.frame.registeredEvents:
            self.unbindRootCommand(actString)
        f = self.views[len(self.views)-1]
        self.__raiseView(f)
        lView.frame.mainframe.destroy()
    

    def bindRootCommand(self, actString, funcEv):
        self.root.bind(actString, funcEv)

    def unbindRootCommand(self, actString):
        self.root.unbind(actString)

    '''
        Remove all views from the stack and destroy current auth and return to the connection view.
    '''
    def disconnect(self):
        self.views = []
        self.auth = Auth("","");
        self.makeConnectView()

    '''
        Prepare the connect view, which is the landing view.
    '''
    def makeConnectView(self):
        cv = ConnectView(self, self.auth)
        self.bindRootCommand('<Return>', cv.regEvent('<Return',lambda x:cv.connBtn()))
        self.stackView(cv)

'''
    
'''

class PaymentPaypalView:
    def __init__(self, appCore, authInfo, tripId):
        self.__appCore = appCore
        self.auth = authInfo
        self.tripId = tripId
        self.frame = FrameInf(self.__appCore, "3 3 12 12")
        self.frame.mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
        self.frame.mainframe.columnconfigure(0, weight=1)
        self.frame.mainframe.rowconfigure(0, weight=1)
        
        ttk.Label(self.frame.mainframe, text='Numéro du compte: ').grid(column=1, row=1, sticky=(W, E))
        self.num_box = ttk.Entry(self.frame.mainframe, width=30)
        self.num_box.grid(column=2, row=1, sticky=(W, E))
        
        self.send_btn = ttk.Button(self.frame.mainframe, text="Envoyer", command=lambda:self.sendBtn()).grid(column=3, row=7, sticky=W)
        self.num_box.focus()
		
    def sendBtn(self):
        if(self.validate()):
            Repository.data.makePaymentPaypal(self.auth.id, self.tripId, self.num_box.get())
            messagebox.showinfo("Succès!", "Transaction complétée avec succès!")
            self.__appCore.unstackView()
            self.__appCore.unstackView()


    def validate(self):
        mess = ''
        if(self.num_box.get() == ''):
            mess += "-Un numéro de carte doit être fourni.\n"
        if(len(self.num_box.get()) != 30):
        	mess += "-Le numéro du compte doit avoir exactement 30 caractères.\n"
        if(mess != ''):
            showMessage(mess)
        return mess == ''

class PaymentCreditView:
    def __init__(self, appCore, authInfo, tripId):
        self.__appCore = appCore
        self.auth = authInfo
        self.tripId = tripId
        self.frame = FrameInf(self.__appCore, "3 3 12 12")
        self.frame.mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
        self.frame.mainframe.columnconfigure(0, weight=1)
        self.frame.mainframe.rowconfigure(0, weight=1)
        
        ttk.Label(self.frame.mainframe, text='Numéro de la carte: ').grid(column=1, row=1, sticky=(W, E))
        self.num_box = ttk.Entry(self.frame.mainframe, width=12)
        self.num_box.grid(column=2, row=1, sticky=(W, E))
        
        ttk.Label(self.frame.mainframe, text='Date d\'expiration').grid(column=1, row=2, sticky=(W, E))
        self.exp_box = ttk.Entry(self.frame.mainframe, width=15)
        self.exp_box.grid(column=2, row=2, sticky=(W, E))
        ttk.Label(self.frame.mainframe, text='(yyyy-MM-dd)').grid(column=3, row=2, sticky=(W,E))
        
        ttk.Label(self.frame.mainframe, text='Nom sur la carte: ').grid(column=1, row=3, sticky=(W, E))
        self.name_box = ttk.Entry(self.frame.mainframe, width=15)
        self.name_box.grid(column=2, row=3, sticky=(W, E))

        ttk.Label(self.frame.mainframe, text='Adresse sur la carte: ').grid(column=1, row=4, sticky=(W, E))
        self.addr_box = ttk.Entry(self.frame.mainframe, width=15)
        self.addr_box.grid(column=2, row=4, sticky=(W, E))

        self.send_btn = ttk.Button(self.frame.mainframe, text="Envoyer", command=lambda:self.sendBtn()).grid(column=3, row=7, sticky=W)
        self.num_box.focus()

    def sendBtn(self):
        if(self.validate()):
            Repository.data.makePaymentCredit(self.auth.id, self.tripId, self.num_box.get(), self.exp_box.get(), self.name_box.get(), self.addr_box.get())
            messagebox.showinfo("Succès!", "Transaction complétée avec succès!")
            self.__appCore.unstackView()
            self.__appCore.unstackView()


    def validate(self):
        mess = ''
        if(self.num_box.get() == ''):
            mess += "-Un numéro de carte doit être fourni.\n"
        if(len(self.num_box.get()) != 12):
        	mess += "-Le numéro de carte doit avoir exactement 12 caractères.\n"
        if(self.name_box.get() == ''):
            mess += '-Un nom doit être fourni.\n'
        if(self.exp_box.get() == ''):
            mess += '-Une date de naissance doit être fournie.\n'
        if(self.addr_box.get() == ''):
            mess += '-Une adresse doit être fournie.\n'
        else:
            try:
                validateDate(self.exp_box.get(), "%Y-%m-%d")
            except ValueError:
                mess += '-La date d\'expiration est invalide.\n'
        if(mess != ''):
            showMessage(mess)
        return mess == ''

class PaymentView:
    def __init__(self, appCore, authInfo, tripId):
        self.__appCore = appCore
        self.auth = authInfo
        self.tripId = tripId
        self.frame = FrameInf(self.__appCore, "3 3 12 12")
        self.frame.mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
        self.frame.mainframe.columnconfigure(0, weight=1)
        self.frame.mainframe.rowconfigure(0, weight=1)
        ttk.Label(self.frame.mainframe, text="Choisir méthode de paiement").grid(column=2, row=0, sticky=(W, E))
        ttk.Button(self.frame.mainframe, text="Crédit (Visa)", command=self.__credit).grid(column=1, row=2, sticky=(W, E))
        ttk.Button(self.frame.mainframe, text="Paypal", command=self.__paypal).grid(column=2, row=2, sticky=(W, E))
        ttk.Button(self.frame.mainframe, text="Annuler", command=self.__cancel).grid(column=3, row=2, sticky=(W, E))

    def __credit(self):
        self.__appCore.stackView(PaymentCreditView(self.__appCore, self.auth, self.tripId))
	
    def __paypal(self):
        self.__appCore.stackView(PaymentPaypalView(self.__appCore, self.auth, self.tripId))
	
    def __cancel(self):
        self.__appCore.unstackView()

    @staticmethod
    def makePayment(appCore, authInfo, tripId):
        #authInfo.id = 1
        #tripId = 1
        paymentStatus = Repository.data.checkPaymentMade(authInfo.id, tripId)
        if not paymentStatus:
            appCore.stackView(PaymentView(appCore, authInfo, tripId))
        else:
            messagebox.showwarning("Erreur!", "Ce paiement a déjà été effectué")

class ConnectView:
    def __init__(self, appCore, authInfo):
        self.__appCore = appCore
        self.auth = authInfo
        self.frame = FrameInf(self.__appCore, "3 3 12 12")
        self.frame.mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
        self.frame.mainframe.columnconfigure(0, weight=1)
        self.frame.mainframe.rowconfigure(0, weight=1)
        self.log_box = ttk.Entry(self.frame.mainframe, width=15, textvariable=self.auth.log)
        self.log_box.grid(column=2, row=1, sticky=(W, E))
        self.pwd_box = ttk.Entry(self.frame.mainframe, width=15, textvariable=self.auth.pwd, show="*")
        self.pwd_box.grid(column=2, row=2, sticky=(W, E))
        self.reg_btn = ttk.Button(self.frame.mainframe, text="S'enregistrer", command=lambda:self.regBtn()).grid(column=2, row=3, sticky=W)
        self.conn_btn = ttk.Button(self.frame.mainframe, text="Connecter", command=lambda:self.connBtn()).grid(column=3, row=3, sticky=W)
        self.log_box.focus()
    
    def connBtn(self):
        self.auth.log = self.log_box.get()
        self.auth.pwd = self.pwd_box.get()
        #print("ConnectView.connBtn : Auth Log is  {} and pwd is {}".format(self.auth.log, self.auth.pwd))
        self.auth = Repository.data.authUser((self.auth.log, self.auth.pwd))
        #update app auth
        self.__appCore.auth = self.auth
        #print(self.auth)
        #print('--------')
        if(self.auth.id > 0):
            self.__appCore.stackView(TripView(self.__appCore, self.auth, [], True, False))
    
    def regBtn(self):
        self.__appCore.stackView(RegisterView(self.__appCore, self.auth))

    def regEvent(self, actString, event):
        self.frame.registeredEvents.append(actString)
        return event

'''
    
'''
class RegisterView:
    def __init__(self, appCore, authInfo):
        self.__appCore = appCore
        self.auth = authInfo
        self.frame = FrameInf(self.__appCore, "3 3 12 12")
        self.frame.mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
        self.frame.mainframe.columnconfigure(0, weight=1)
        self.frame.mainframe.rowconfigure(0, weight=1)
        
        ttk.Label(self.frame.mainframe, text='Nom utilisateur: ').grid(column=1, row=1, sticky=(W, E))
        self.log_box = ttk.Entry(self.frame.mainframe, width=15, textvariable=self.auth.log)
        self.log_box.grid(column=2, row=1, sticky=(W, E))
        
        ttk.Label(self.frame.mainframe, text='Mot de passe: ').grid(column=1, row=2, sticky=(W, E))
        self.pwd_box = ttk.Entry(self.frame.mainframe, width=15, textvariable=self.auth.pwd, show="*")
        self.pwd_box.grid(column=2, row=2, sticky=(W, E))
        
        ttk.Label(self.frame.mainframe, text='Confirmation: ').grid(column=1, row=3, sticky=(W, E))
        self.pwdConfirm_box = ttk.Entry(self.frame.mainframe, width=15, textvariable=self.auth.pwd, show="*")
        self.pwdConfirm_box.grid(column=2, row=3, sticky=(W, E))

        ttk.Label(self.frame.mainframe, text='Nom complet: ').grid(column=1, row=4, sticky=(W, E))
        self.name_box = ttk.Entry(self.frame.mainframe, width=15, textvariable=self.auth.name)
        self.name_box.grid(column=2, row=4, sticky=(W, E))
        
        ttk.Label(self.frame.mainframe, text='Adresse courriel: ').grid(column=1, row=5, sticky=(W, E))
        self.email_box = ttk.Entry(self.frame.mainframe, width=15, textvariable=self.auth.email)
        self.email_box.grid(column=2, row=5, sticky=(W, E))

        ttk.Label(self.frame.mainframe, text='Date naissance: ').grid(column=1, row=6, sticky=(W, E))
        self.birthD_box = ttk.Entry(self.frame.mainframe, width=15, textvariable=self.auth.email)
        self.birthD_box.grid(column=2, row=6, sticky=(W, E))
        ttk.Label(self.frame.mainframe, text='(yyyy-MM-dd)').grid(column=3, row=6, sticky=(W,E))

        self.send_btn = ttk.Button(self.frame.mainframe, text="Envoyer", command=lambda:self.sendBtn()).grid(column=3, row=7, sticky=W)
        self.log_box.focus()

    def sendBtn(self):
        if(self.validate()):
            self.auth.log = self.log_box.get()
            self.auth.pwd = self.pwd_box.get()
            self.auth.name = self.name_box.get()
            self.auth.email = self.email_box.get()
            self.auth.birthDay = self.birthD_box.get()
            #print(self.auth)
            res = Repository.data.checkIfUserExists(self.log_box.get(), self.email_box.get())
            if(res):
                showMessage("Le nom d'utilisateur ou/et l'adresse de messagerie saisie ne sont pas disponibles.")
                return;
            r = Repository.data.regUser(self.auth)
            if(self.auth.id > 0):
                showMessage('Le compte {} a été créé avec succès. '.format(self.auth.log))
                self.__appCore.disconnect()
            else:
                showMessage('La création du compte {} a échoué.'.format(self.auth.log))


    def validate(self):
        mess = ''
        if(self.log_box.get() == ''):
            mess += "-Un nom d'utilisateur doit être fourni.\n"
        if(self.email_box.get() == ''):
            mess += '-Une adresse de messagerie doit être fournie.\n'
        if(self.birthD_box.get() == ''):
            mess += '-Une date de naissance doit être fournie.\n'
        else:
            try:
                validateDate(self.birthD_box.get(), "%Y-%m-%d")
            except ValueError:
                mess += '-La date de naissance est invalide.\n'
        if(self.pwd_box.get() != self.pwdConfirm_box.get()):
            mess += '-Les mots de passe fournis ne sont pas identiques.\n'
        if(mess != ''):
            showMessage(mess)
        #print('RegisterView.validate: Mess value is = {}'.format(mess))
        return mess == ''

class TripView:
    def __init__(self, appCore, authInfo, filters, hasFilters, dispPayGrid):
        self.__appCore = appCore
        self.auth = authInfo
        if(dispPayGrid):
            self.prepareTripDisplay(Repository.data.fetchTripAndPaidStatus(self.auth), hasFilters, dispPayGrid)        
        else:
            self.prepareTripDisplay(Repository.data.fetchTripsForUser(self.auth), hasFilters, dispPayGrid)

    def prepareTripDisplay(self, trips, hasFilters, dispPayGrid):
        self.frame = FrameInf(self.__appCore, "3 3 12 12")
        self.frame.mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
        rowIdx = 1
        if(hasFilters):
            self.makeFilterZone(rowIdx)
        rowIdx +=1
        self.makeHeaders(rowIdx)
        self.makeTripGrid(trips, rowIdx, dispPayGrid)

    def displayStops(self, trip):
        print('---------------------------------')
        for stop in trip.stops:
            print(stop)

    def makeFilterZone(self, rowIdx):
        ttk.Label(self.frame.mainframe, text='Date voyage : ').grid(column=1, row=rowIdx, sticky=(W, E))
        self.tripDate_Box = ttk.Entry(self.frame.mainframe, width=15, textvariable='')
        self.tripDate_Box.grid(column=2, row=rowIdx, sticky=(W, E))
        ttk.Label(self.frame.mainframe, text='(yyyy-MM-dd)').grid(column=3, row=rowIdx, sticky=(W, E))
        ttk.Label(self.frame.mainframe, text='Sièges disponibles : ').grid(column=4, row=rowIdx, sticky=(W, E))
        self.availSeat_box = ttk.Entry(self.frame.mainframe, width=5, textvariable='')
        self.availSeat_box.grid(column=5, row=rowIdx)
        ttk.Button(self.frame.mainframe, text="Filtrer", command= lambda: self.filterTripsBtn()).grid(column=6, row=rowIdx, sticky=W)
    
    def makeHeaders(self, rowIdx):
        ttk.Label(self.frame.mainframe, text='Adresse départ').grid(column=1, row=rowIdx, sticky=(W, E))
        ttk.Label(self.frame.mainframe, text='').grid(column=2, row=rowIdx, sticky=(W, E))
        ttk.Label(self.frame.mainframe, text='Adresse arrivée').grid(column=3, row=rowIdx, sticky=(W, E))
        ttk.Label(self.frame.mainframe, text='distance').grid(column=4, row=rowIdx, sticky=(W, E))
        ttk.Label(self.frame.mainframe, text='date').grid(column=5, row=rowIdx, sticky=(W, E))

    def makeTripGrid(self, trips, rowIdx, dispPayGrid):
        for trip in trips:
            rowIdx += 1
            colIdx = 1
            fromAddress = trip.stops[0].address
            toAddress = trip.stops[len(trip.stops) - 1].address
            ttk.Label(self.frame.mainframe, text=fromAddress, borderwidth=1, relief='solid').grid(column=colIdx, row=rowIdx, sticky=(W, E))
            colIdx += 1
            ttk.Label(self.frame.mainframe, text=' => ').grid(column=colIdx, row=rowIdx, sticky=(W, E))
            colIdx += 1
            ttk.Label(self.frame.mainframe, text=toAddress, borderwidth=1, relief='solid').grid(column=colIdx, row=rowIdx, sticky=(W, E))
            colIdx += 1
            ttk.Label(self.frame.mainframe, text=trip.totalDistance, borderwidth=1, relief='solid').grid(column=colIdx, row=rowIdx, sticky=(W, E))
            colIdx += 1
            ttk.Label(self.frame.mainframe, text=trip.date, borderwidth=1, relief='solid').grid(column=colIdx, row=rowIdx, sticky=(W, E))
            colIdx += 1
            if(dispPayGrid == False):
                ttk.Button(self.frame.mainframe, text="Arrêts", command=lambda trip=trip:self.displayStops(trip)).grid(column=colIdx, row=rowIdx, sticky=W)
            else:
                ttk.Button(self.frame.mainframe, text="Payer", command=lambda trip=trip:self.makePayView(trip)).grid(column=colIdx, row=rowIdx, sticky=W)


    def filterTripsBtn(self):
        tripDate = self.tripDate_Box.get()
        seatCount = self.availSeat_box.get()
        try:
            sqlFilters = []
            if(tripDate != ''):
                validateDate(tripDate, "%Y-%m-%d")
                sqlFilters.append(SqlFilter("AND", tripDate, "t.date", "=", ' {} {} {} %s'))
            if(seatCount != ''):
                sqlFilters.append(SqlFilter("AND", seatCount, "", "+", " {} ((SELECT IFNULL(SUM(reservedSeatsNb),0) FROM Travels as ut WHERE ut.tripId = t.id) + %s) < (SELECT seatsNb FROM Car WHERE id = t.carId)"))
            self.prepareTripDisplay(Repository.data.fetchFilteredTripsForUser(self.auth, sqlFilters), True, False)
        except ValueError:
            showMessage('-Le format de la date est invalide.\n')

    def makePayView(self, trip):
        PaymentView.makePayment(self.__appCore, self.auth, trip.id)
        #self.__appCore.stackView(PaymentView())


'''
    Shared components (ttk.Frame, menuBar, events)
    mainFrame : all views use a frame (displayed in app window)
    events : all views can register events. We keep a list of those to remove later
    menuBar: all views have a menubar
'''
class FrameInf:
    def __init__(self, appCore, paddingConf):
        self.mainframe = ttk.Frame(appCore.root, padding=paddingConf)
        self.registeredEvents = []
        self.menuBar = MenuBarWidget(appCore, self.mainframe)
    
    def regEvent(self, actString, event):
        self.registeredEvents.push[actString]
        return event


'''
   -- All views use MenuBar. If the user is auth, we had the option to disconnect form the app.
'''

class MenuBarWidget:
    def __init__(self, appCore, frame):
        print('MenuBarInit : {}'.format(appCore.auth))
        self.__appCore = appCore
        self.__frame = frame
        menubar = Menu(appCore.root)
        menu_file = Menu(menubar)
        menu_edit = Menu(menubar)
        menubar.add_cascade(menu=menu_file, label='Menu', command=lambda:self.disconnectBtn())
        if(appCore.auth.id > 0):
            menu_file.add_command(label="Trajets disponibles", command=lambda:self.availableTripBtn())
            menu_file.add_command(label="État compte", command=lambda:self.accountStateBtn())
            menu_file.add_command(label="Déconnecter", command=lambda:self.disconnectBtn())
        appCore.root['menu'] = menubar

    def disconnectBtn(self):
        self.__appCore.disconnect()

    def availableTripBtn(self):
        self.__appCore.unstackView()
        self.__appCore.stackView(TripView(self.__appCore, self.__appCore.auth, [], True, False))

    def accountStateBtn(self):
        self.__appCore.unstackView()
        self.__appCore.stackView(TripView(self.__appCore, self.__appCore.auth, [], False, True))
